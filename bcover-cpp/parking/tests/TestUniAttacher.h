#ifndef __TESTUNIATTACHER__
#define __TESTUNIATTACHER__

#include "UniAttacher.h"
#include <gtest/gtest.h>
#include <vector>


std::vector<double> getFreePos(double pStart, double pEnd,double carLength, size_t minPos)
{
	  ParkingLot<double> myLot (pStart,pEnd);
	  UniAttacher<double> attacher(carLength, myLot); 
	  
	  size_t count = 0;

  	  while(count <= minPos)
	  {
		count += attacher.tryAttach();
	  }
  	 
	  std::vector<double> shifted_mid;
	   auto occ = myLot.getLot();
	   double c = 0.0;
  	for(auto i = occ.begin(); i != occ.end() ;++i)
  	{
		auto mid = (i->lower() + i->upper()) / 2.0;		
		shifted_mid.push_back( mid - c);
		c += carLength;
  	}  	

	return shifted_mid;  
}

void addTo(std::vector<double>& v1, std::vector<size_t> &counts, std::vector<double> inc)
{
	size_t l1 = v1.size(), l2 = inc.size();
	
	size_t i = 0;
	for (; i != std::min(l1,l2); ++i)
	{
		v1.at(i) += inc.at(i);
		++counts.at(i);
	}
	if (l2 >= l1)
	{
		for (size_t j = i; j != inc.size(); ++j)
		{
			v1.push_back(inc.at(j)); 
			counts.push_back(1);
		}	
	}

}


TEST(UniAttacher, Through) 
{       
  double carLength = 0.05;
  double pStart = 0.0, pEnd = 1.0;
  

  size_t minPos = 12;
  size_t loops = 500;

  std::vector <double> positions;
  std::vector <size_t> counts;


  for(size_t i = 0; i != loops ; ++i)
  {
	
	std::vector<double> curPos = getFreePos(pStart,pEnd,carLength, minPos);
	//std::cout << curPos.size() << " ";
	addTo(positions,counts,curPos);	
  }
 
  std::cout << "\nminPos = "<< minPos ; 
  std::cout << std::endl << "\nfree position mean:";
  std::cout << "free slack = "<< (pEnd - pStart) - carLength * minPos;
  std::cout << "\n min and max " << *std::min_element(counts.begin(), counts.end()) << ", " << *std::max_element(counts.begin(), counts.end()) << "\n";
  for(size_t i = 0; i != minPos ; ++i)
  {
	std::cout << positions.at(i) / minPos  << " ";
  }	 	
	
  	 	
}


#endif
