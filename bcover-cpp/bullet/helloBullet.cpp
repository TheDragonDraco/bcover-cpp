#include <btBulletDynamicsCommon.h>
#include <iostream>
#include <memory>
int main () {
    std::cout << "Hello World!" << std::endl;

    // Build the broadphase
    std::unique_ptr<btBroadphaseInterface> broadphase (new btDbvtBroadphase());

    // Set up the collision configuration and dispatcher
    std::unique_ptr<btDefaultCollisionConfiguration> collisionConfiguration (new btDefaultCollisionConfiguration());
    std::unique_ptr<btCollisionDispatcher> dispatcher (new btCollisionDispatcher(collisionConfiguration.get()));

    // The actual physics solver
    std::unique_ptr<btSequentialImpulseConstraintSolver> solver (new btSequentialImpulseConstraintSolver);

    // The world.
    std::unique_ptr<btDynamicsWorld> dynamicsWorld ( new btDiscreteDynamicsWorld(dispatcher.get(), broadphase.get(), solver.get(), collisionConfiguration.get()));
    dynamicsWorld->setGravity(btVector3(0, -10, 0));

    // Do_everything_else_here
    btCollisionShape* shape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);

    btDefaultMotionState* motionState = new btDefaultMotionState(btTransform(btQuaternion(0,0,0,1), btVector3(0,-1,0)));
    btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(
	0,                  // mass
	motionState,        // initial position
	shape,              // collision shape of body
	btVector3(0,0,0)    // local inertia
    );

    btRigidBody *rigidBody = new btRigidBody(rigidBodyCI);
    dynamicsWorld->addRigidBody(rigidBody);


    // Clean up behind ourselves like good little programmers
   
    return 0;
}
