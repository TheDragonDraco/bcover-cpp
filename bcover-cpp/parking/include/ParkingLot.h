#ifndef	 __PARKINGLOT__
#define __PARKINGLOT__

#include <boost/icl/interval.hpp>
#include <boost/icl/interval_set.hpp>
#include <algorithm>
#include <typeinfo>
#include <mutex>
#include "Car.h"

template<typename Numtype = double>
class ParkingLot  
{
  private:
  boost::icl::continuous_interval<Numtype, std::less> inter;
  boost::icl::interval_set<Numtype> occupied;
  std::mutex mtx;
  
  public:

  ParkingLot(Numtype const &end1 , Numtype const &end2) 
  { 
    this->inter =  boost::icl::interval<Numtype>::closed(end1,end2);     
  }  
  
  ParkingLot(ParkingLot const & other) : inter(other.inter), occupied (other.occupied) {}
  
  Inttype<Numtype> getInter() const { return inter;} 
  
  // Will a new car have collisions?
  bool hasCollisions(Car<Numtype> &  C) const
  {
    
    auto iters = occupied.equal_range(C.getInter());
    return std::distance(iters.first, iters.second);
  }
  
  ParkingLot& insert(Car<Numtype> V)
  {

    if( boost::icl::contains(inter, V.getInter())  && !hasCollisions(V))
    {
      mtx.lock();
      occupied.add(V.getInter());
      mtx.unlock();
      
    }
     return *this;
  }
  
  ParkingLot& remove(Car<Numtype> V)
  { 
    bool rv = boost::icl::contains(occupied,V.getInter());
    if (rv)
    {
      mtx.lock();
      occupied -= V.getInter();
      mtx.unlock();
      
    }
     return *this;
  }
    
  size_t getNumCars() const
  {
    return std::distance(occupied.begin(), occupied.end());    
  }
  
  std::string toStr() const
  {
     std::stringstream o; 
     for(auto iter = occupied.begin(); iter != occupied.end(); ++iter) 
          o<< *iter <<" ";
     
     std::string str;
     o >> str;
     
     return str;
     
  }
  
  friend std::ostream & operator <<(std::ostream  &o, ParkingLot<Numtype>  const &myLot) 
  {
    return o<< myLot.toStr(); 
  }
  
  auto getLot() const { return occupied;} 
  
};

#endif
