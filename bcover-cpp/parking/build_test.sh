
LIBNAME=libParkWrapper.so
g++ -shared -Wl,-soname,"$LIBNAME" -std=c++1y -L/usr/local/lib -I/usr/include/python2.7 -I./include/ tests/TestPython.cpp  -lboost_python -fpic -o $LIBNAME
mv $LIBNAME tests/
python tests/TestPython.py
