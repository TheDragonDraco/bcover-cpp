#include "gazebo.hh"
#include "common/common.hh"
#include "transport/transport.hh"
#include "physics/physics.hh"
#include "sdf/sdf.hh"
#include "msgs/msgs.hh"

namespace gazebo
{
  class simple : public WorldPlugin
  {
    public: void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
    {
      // Create a new transport node
      transport::NodePtr node(new transport::Node());

      // Initialize the node with the world name
      node->Init(_parent->GetName());

      // Create a publisher on the ~/physics topic
      transport::PublisherPtr physicsPub =
        node->Advertise<msgs::Physics>("~/physics");

      msgs::Physics physicsMsg;
      physicsMsg.set_type(msgs::Physics::BULLET);

      // Set the step time
      //physicsMsg.set_dt(0.01);

      // Change gravity
      msgs::Set(physicsMsg.mutable_gravity(), math::Vector3(0.01, 0, -9.81));
      physicsPub->Publish(physicsMsg);
    }
  };

  // Register this plugin with the simulator
  GZ_REGISTER_WORLD_PLUGIN(simple)
}