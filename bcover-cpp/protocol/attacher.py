#!/usr/bin/python3
import numpy 
class Attacher:
	positions = [] # array of robot positions
	slacks = []
	robots = 0 # Number of robots
	length = 0 # Boundary length
	driver = None

	def __init__(self, robots, length):
		self.robots = robots
		self.length = length

		self.positions = [0, length]
		self.slacks = numpy.diff(self.positions).tolist() 
		self.driver = self.divideLongestSlack

	def divideLongestSlack(self):
		ind = self.slacks.index(max(self.slacks))
		# print("ind = ", ind)

		newpos = numpy.random.rand() * self.slacks[ind] + self.positions[ind]
		self.positions.insert(ind + 1,newpos)
		self.slacks = numpy.diff(self.positions).tolist()
		# print(self.positions)

	def attachAll(self):
		for i in range(self.robots):
			self.driver()
	



