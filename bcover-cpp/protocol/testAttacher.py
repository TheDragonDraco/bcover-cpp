#!/usr/bin/python3
from attacher import Attacher
import numpy
import scipy
from matplotlib import pyplot

def test(robots, length):
	Att = Attacher(robots, length)
	Att.attachAll()

	nparr = numpy.array([Att.positions])

	for i in range(10000):
		Att = Attacher(robots, length)
		Att.attachAll()
		nparr = numpy.concatenate((nparr, [Att.positions]),axis = 0)	
	for i in range(1,robots):
		pyplot.hist(nparr[:,i])
		pyplot.xlabel(str(i) + '/' + str(robots))	
		pyplot.show()
	return [numpy.mean(nparr, axis = 0), numpy.var(nparr, axis = 0), numpy.cov(nparr.T)]

for i in range(4,5):
	mvar =  test(i,1)
	print('Robots:', i)
	print('Means:', mvar[0])
	print('Variances:', mvar[1])
	print('Covariances:', mvar[2])
