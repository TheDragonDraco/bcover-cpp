#ifndef __UNIATTACH__
#define __UNIATTACH__

#include "ParkingLot.h"
#include <boost/random.hpp>
#include <chrono>
#include <memory>
//Attaches uniformly to parking lot
template<typename Numtype>
using Ptrtype = std::shared_ptr<ParkingLot<Numtype>>; 



template<typename Numtype> 
class UniAttacher 
{
  private:
   Numtype carLength;
   ParkingLot<Numtype> &pLot;
   boost::uniform_real<double> uni;
   boost::hellekalek1995 rngG;
   boost::variate_generator<boost::hellekalek1995&, boost::uniform_real<>> puniRngG;
   
   
  public:
    constexpr static const long double parkingConstant = 0.74759792025; //..341143517873094363652421026172;

    
    UniAttacher(Numtype carLength2, ParkingLot<Numtype> &pLot2): carLength(carLength2), 
								 pLot(pLot2), 
								 uni(boost::uniform_real<>( boost::icl::lower(pLot.getInter()),
								 boost::icl::upper(pLot.getInter()) - carLength)), puniRngG(rngG,uni)
    {     
	  rngG.seed(std::time(0));    
    }
    
    static double getParkingConstant() { return parkingConstant;} // For Boost Python
    
    Numtype getCarLength() const { return carLength;}
    
    ParkingLot<Numtype> & getPLot() const { return pLot;}
    

    
    bool tryAttach()
    {
      size_t parkedCars = pLot.getNumCars();
      
   
      Numtype leftend = puniRngG();	  
      Inttype<Numtype> randInter = boost::icl::interval<Numtype>::open(leftend, leftend + carLength);       
      pLot.insert(Car<Numtype>(randInter));
      
      return (pLot.getNumCars() > parkedCars);
      
    }
  
  
};

#endif