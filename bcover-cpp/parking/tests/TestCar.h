#ifndef __TESTCAR__
#define __TESTCAR__

#include "ParkingLot.h"
#include "WrapCar.h"
#include <gtest/gtest.h>

TEST(Car, Constructor) 
{     
  Inttype<double> inter1 =  boost::icl::interval<double>::open(0.0,0.5); 
  Inttype<double> inter2 =  boost::icl::interval<double>::open(1,1.5);
  Inttype<double> inter3 =  boost::icl::interval<double>::open(.25,1.25);
  std::cout <<inter2 << (inter2 == inter1);
  std::cout <<inter1 << std::endl;
 
  Car<double> CarArr[] = { inter1,inter2, inter3};


  std::cout<<"\n"<<CarArr[0] <<"\t"<<CarArr[1];

 
  ParkingLot<double> myLot(0,10);
  myLot.insert(CarArr[0]).insert(CarArr[1]).insert(CarArr[2]).remove(CarArr[0]);
  std::cout<<myLot;  
}

#endif