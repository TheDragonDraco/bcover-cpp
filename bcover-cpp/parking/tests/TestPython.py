#!/usr/bin/python
import sys
import unittest

# Add 2 possible Paths
sys.path.append('lib')
sys.path.append('../lib')

from libParkWrapper import Inter, Car, ParkingLot, UniAttacher


class TestAllClasses(unittest.TestCase):
  
  
  def setUp(self):
      # Interval test
      self.I = Inter(5,6)
      self.C = Car(6.0, 7.0)
      self.C2 = Car(7.0,8.0)
      self.P = ParkingLot(0,1000)
      self.UnAtt = UniAttacher(1,self.P)
      self.tolerance = 0.001
      
  def testInterval(self):
      self.assertAlmostEqual(self.I.lower() , 5) # These are doubles! 
      self.assertAlmostEqual(self.I.upper() , 6)

  def testCar(self):
      self.assertAlmostEqual(self.C.getLength() , 1)
      self.assertTrue(self.C == self.C)
      
      self.assertTrue(self.C2 > self.C)
      self.assertTrue(self.C < self.C2)
      
  def testParkingLot(self):
      self.assertTrue(self.P.getNumCars() == 0)
      
      self.P.insert(self.C)
      self.assertTrue(self.P.getNumCars() == 1)
      
      self.P.insert(self.C2)
      self.assertTrue(self.P.getNumCars() == 2)

      self.P.remove(self.C2)
      self.assertTrue(self.P.getNumCars() == 1)
      
  def testUniAttacher(self):
      self.assertTrue(self.UnAtt.getCarLength() == 1)
      self.assertAlmostEqual(self.UnAtt.getParkingConstant(), 0.75,2)
      
      
      for i in xrange(20000):
	  self.UnAtt.tryAttach()
	  
      print '# of Attached Cars = ', self.P.getNumCars()      


if __name__ == '__main__':
    unittest.main()