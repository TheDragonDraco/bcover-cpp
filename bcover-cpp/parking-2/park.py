#!/usr/bin/python
import numpy as np
from numpy.random import uniform
import matplotlib.pyplot as plt 

def choose2Positions(s,Delta):
	
	pos = [ uniform(0,1) * (s - Delta), 0.0]
	while(True):
		pos[1] = uniform(0,1) * (s-Delta)
		if (abs(pos[1] - pos[0]) >= Delta):
			break	 

	
	if (pos[1] <= pos[0]):
		print "!"	
	assert(abs(pos[1] - pos[0]) >= Delta)	

	pos2 = [min(pos), max(pos)]
	assert(pos2[0] <= pos2[1] - Delta)
	assert(pos2[0] >= 0)
	assert(pos2[1] <= s - Delta)

	return pos

s = 10
Delta = 2.5
mean = [0,0]
posarr = [[0],[0] ]
trials = 50000

for i in range(trials):
	pos = choose2Positions(s,Delta)
	mean[0] += pos[0]
	mean[1] += pos[1]
	posarr[0].append(pos[0])
	posarr[1].append(pos[1])

mean[0] = mean[0] / trials
mean[1] = mean[1] / trials

print mean 
#print posarr[1]

colors = ['b','r']
for i in range(2):
	plt.figure(i + 1)
	count,bins,ignored = plt.hist(posarr[i])	
	plt.plot(bins, np.ones_like(bins), linewidth = 1, color = colors[i])

plt.show()	




