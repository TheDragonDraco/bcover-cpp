#ifndef	 __CAR__
#define __CAR__

#include <boost/icl/interval.hpp>
#include <boost/icl/continuous_interval.hpp>
#include <boost/icl/interval_bounds.hpp>


#include <algorithm>
#include <typeinfo>
#include <iostream>
#include <string>
#include <sstream>

template<typename Numtype>
using Inttype = boost::icl::continuous_interval<Numtype, std::less> ;

template<typename Numtype>
struct intToStr
{
  static std::string toStr (boost::icl::continuous_interval<Numtype,std::less> const &Int) 
  { 
    std::stringstream o; 
    o <<"["<< boost::icl::lower(Int)<<"," <<boost::icl::upper(Int) <<"]" ; 
  
    std::string str;
    o >> str;
  
    return str;
  }  
 
};


			  
			  
template<typename Numtype =  double>
class Car  
{
 private:
     Inttype<Numtype> inter;
  
 public:
  
    Car() {;}
    
    Car(Numtype end1, Numtype end2)
    {
      this->inter =  boost::icl::interval<Numtype>::open(end1,end2);       
    }
      
    Car(boost::icl::continuous_interval<Numtype> &  inter2 ):inter(inter2) 
    { 
      //std::cout << "\ninterval = "<< inter;  
    }
    
    Numtype getLength() const { return boost::icl::length(inter);}
    
    bool operator < (Car<Numtype> const & other) const { return getInter() < other.getInter();}
    
    bool operator == (Car<Numtype> const & other) const { return getInter() == other.getInter();}
    
    bool operator > (Car<Numtype> const & other) const  { return getInter() > other.getInter();}
    
    boost::icl::continuous_interval<Numtype,std::less>  getInter() const { return inter;}  
    
    std::string toStr() const { return  intToStr<Numtype>::toStr(getInter()); }   
    
     ~Car() { }
    
};

template<typename Numtype = double>
std::ostream &operator << (std::ostream &o, Car<Numtype> const &mycar) { return o << mycar.getInter();}  


#endif