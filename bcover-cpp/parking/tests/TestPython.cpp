#include <boost/python.hpp>
#include <boost/python/return_internal_reference.hpp>
#include "ParkingLot.h"
#include "UniAttacher.h"



BOOST_PYTHON_MODULE(libParkWrapper)
{
    using namespace boost::python;
    //def ("Car", Car<double>);
    class_<boost::icl::continuous_interval<double, std::less>> ("Inter", init<double,double>()) 
    .def("lower", &boost::icl::continuous_interval<double, std::less>::lower)
    .def("upper", &boost::icl::continuous_interval<double, std::less>::upper)
    .def("tostr", &intToStr<double>::toStr)
    ;
       
    
    class_<Car<double>>("Car", init<double,double>())
    //.def(init<boost::icl::continuous_interval<double,std::less>>())
    .def("getLength", &Car<double>::getLength)
    .def("getInter", &Car<double>::getInter)
    .def("tostr", &Car<double>::toStr)
    .def(self < self)
    .def(self == self)
    .def(self > self)    
    ;
    
    
    class_<ParkingLot<double>>("ParkingLot", init<double,double>()) 
    .def("getInter", &ParkingLot<double>::getInter)
    .def("hasCollisions", &ParkingLot<double>::hasCollisions)
    .def("insert",&ParkingLot<double>::insert, return_internal_reference<>())
    .def("remove", (&ParkingLot<double>::remove), return_internal_reference<>())
    .def("getNumCars",&ParkingLot<double>::getNumCars)
    .def("toStr", &ParkingLot<double>::toStr)
    ;
    
     class_<UniAttacher<double>>("UniAttacher", init<double,ParkingLot<double>&>()) 
    .def("getParkingConstant", &UniAttacher<double>::getParkingConstant).staticmethod("getParkingConstant")    
    .def("getCarLength", &UniAttacher<double>::getCarLength)
    .def("getPLot", &UniAttacher<double>::getPLot, return_internal_reference<>() )
    .def("tryAttach",&UniAttacher<double>::tryAttach)
    ;
}


